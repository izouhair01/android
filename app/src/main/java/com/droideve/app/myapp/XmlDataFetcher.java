package com.droideve.app.myapp;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class XmlDataFetcher implements Runnable {

    private final InputStream flux;
    private String result = "";

    public XmlDataFetcher(InputStream flux) {
        this.flux = flux;
    }

    @Override
    public void run() {
        try {

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = null;
            try {
                dBuilder = dbFactory.newDocumentBuilder();
                Document doc = dBuilder.parse(flux);

                Element element = doc.getDocumentElement();
                element.normalize();

                NodeList nList = doc.getElementsByTagName("package");

                for (int i = 0; i < nList.getLength(); i++) {

                    Node node = nList.item(i);
                    if (node.getNodeType() == Node.ELEMENT_NODE) {
                        Element element2 = (Element) node;
                        if (element2 != null) {
                            result = result+" \n" + "Name " + element2.getAttribute("name") + "  - Location " + element2.getAttribute("codePath") + "\n";
                        }

                    }
                }


            } catch (ParserConfigurationException | SAXException e) {
                e.printStackTrace();
            }


        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }


    public String getResult() {
        return result;
    }
}
