package com.droideve.app.myapp;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.topjohnwu.superuser.Shell;
import com.topjohnwu.superuser.io.SuFile;
import com.topjohnwu.superuser.io.SuFileInputStream;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public class MainActivity extends AppCompatActivity {


    public static final String SOURCE_FILE_NAME = "packages.xml";
    public static final String SOURCE_PATH = "/data/system/";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //make sure the source path is correct
        File dir = new File(SOURCE_PATH + SOURCE_FILE_NAME);
        if (dir.exists()) {

            File bootBlock = SuFile.open( SOURCE_PATH + SOURCE_FILE_NAME);
            if (bootBlock.exists()) {

                try (InputStream in = SuFileInputStream.open(bootBlock)) {
                    XmlDataFetcher threadRetriever = new XmlDataFetcher(in);
                    Thread thread = new Thread(threadRetriever);
                    thread.start();
                    thread.join();

                    //display the result on a text view
                    ((TextView) findViewById(R.id.container)).setText(threadRetriever.getResult());

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }


        }


    }


}