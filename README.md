# PackagesDataRetriever


Please read the task explanation below.


Create an Android application which has the simplest UI for displaying text area.
On the run it should display some data collected by parsing an XML file located at
'/data/system/packages.xml' which is accessible only for privileged users.

The format of this file is next:
<?xml version='1.0' encoding='utf-8' standalone='yes' ?>
<packages>
   ....
   <package name="com.sec.android.app.DataCreate" codePath="/system/app/AutomationTest_FB" nativeLibraryPath="/system/app/AutomationTest_FB/lib" publicFlags="537443909" privateFlags="0" ft="11e8d8ee980" it="11e8d8ee980" ut="11e8d8ee980" version="1" userId="10217" isOrphaned="true">
    .....
</packages>

Extracted data should contains list of all application names ('name' property)
and their locations ('codePath' property)

Code should iterate over all <package> entries and collect required properties.


Remarks:


1. For retrieving content of file Runtime.getRuntime().exec() can be used with proper command and proper
input and output streams processing. As a more advanced option https://github.com/topjohnwu/libsu
library can be used as an helper for root commands execution.


2. Do not use external libraries for XML parsing (only standard built-in java functionality).
